﻿using Verse;
using RimWorld;
using UnityEngine;

using System.IO;

namespace CustomMusic {
	public class Dialog_AdvancedSettings : Window {
		private readonly CustomSongDef song = null;
		protected float margin = 4f;
		protected float lineHeight = 24f;
		protected float lineSkip = 24f;
		protected bool spring;
		protected bool summer;
		protected bool fall;
		protected bool winter;
		protected bool perm_summer;
		protected bool perm_winter;

		public Dialog_AdvancedSettings(CustomSongDef song) {
			this.doCloseX = true;
			this.doCloseButton = true;
			this.forcePause = true;
			this.absorbInputAroundWindow = true;

			this.song = song;
			this.spring = song.PlaysDuringSpring;
			this.summer = song.PlaysDuringSummer;
			this.fall = song.PlaysDuringFall;
			this.winter = song.PlaysDuringWinter;
			this.perm_summer = song.PlaysDuringPermSummer;
			this.perm_winter = song.PlaysDuringPermWinter;
			if (!(spring || summer || fall || winter || perm_summer || perm_winter))
				this.spring = this.summer = this.fall = this.winter = this.perm_summer = this.perm_winter = true;
		}

		public override void DoWindowContents(Rect inRect) {
			if (this.song == null)
				this.Close(false);
			// Header
			string name = Path.GetFileNameWithoutExtension(this.song.clipPath);
			Text.Font = GameFont.Medium;
			Text.Anchor = TextAnchor.MiddleCenter;
			Vector2 nameSize = Text.CalcSize(name);
			Widgets.Label(new Rect(inRect.x + inRect.width / 2 - nameSize.x / 2, inRect.y, nameSize.x, nameSize.y),
						  name);
			Text.Font = GameFont.Small;
			// Left column
			Rect left = inRect.LeftHalf();
			left.y += nameSize.y + margin;
			left.width -= margin / 2;
			left.height -= nameSize.y + margin;
			GUI.BeginGroup(left);
			Text.Anchor = TextAnchor.MiddleLeft;
			Widgets.Label(new Rect(0, 0, left.width, lineHeight), "CustomMusic_allowedSeasons".Translate());
			Widgets.Label(new Rect(0, lineHeight * 3 + lineSkip, left.width, lineHeight),
						  "CustomMusic_allowedTimeOfDay".Translate());
			Widgets.Label(new Rect(0, lineHeight * 4 + lineSkip * 2, left.width, lineHeight),
						  "CustomMusic_tense".Translate());
			Widgets.Label(new Rect(0, lineHeight * 5 + lineSkip * 3, left.width, lineHeight),
						  "CustomMusic_commonality".Translate());
			Widgets.Label(new Rect(0, lineHeight * 6 + lineSkip * 4, left.width, lineHeight),
						  "CustomMusic_playOnMap".Translate());
			Widgets.Label(new Rect(0, lineHeight * 7 + lineSkip * 5, left.width, lineHeight),
						  "CustomMusic_volume".Translate());
			Text.Anchor = TextAnchor.UpperLeft;
			GUI.EndGroup();

			// Right column
			Rect right = inRect.RightHalf();
			right.x += margin / 2;
			right.y += nameSize.y + margin;
			right.width -= margin / 2;
			right.height -= nameSize.y + margin;
			GUI.BeginGroup(right);
			// Allowed seasons
			{
				// Spring
				string labelSpring = SeasonUtility.LabelCap(Season.Spring);
				float labelSpringWidth = Text.CalcSize(labelSpring).x + 28f;
				Widgets.CheckboxLabeled(new Rect(right.width / 2 - labelSpringWidth, 0, labelSpringWidth, lineHeight),
										labelSpring, ref spring);
				// Summer
				string labelSummer = SeasonUtility.LabelCap(Season.Summer);
				float labelSummerWidth = Text.CalcSize(labelSummer).x + 28f;
				Widgets.CheckboxLabeled(new Rect(right.width - labelSummerWidth, 0, labelSummerWidth, lineHeight),
										labelSummer, ref summer);
				// Fall
				string labelFall = SeasonUtility.LabelCap(Season.Fall);
				float labelFallWidth = Text.CalcSize(labelFall).x + 28f;
				Widgets.CheckboxLabeled(new Rect(right.width / 2 - labelFallWidth, lineHeight, labelFallWidth, lineHeight),
										labelFall, ref fall);
				// Winter
				string labelWinter = SeasonUtility.LabelCap(Season.Winter);
				float labelWinterWidth = Text.CalcSize(labelWinter).x + 28f;
				Widgets.CheckboxLabeled(new Rect(right.width - labelWinterWidth, lineHeight, labelWinterWidth, lineHeight),
										labelWinter, ref winter);
				// Permanent summer
				string labelPermSummer = SeasonUtility.LabelCap(Season.PermanentSummer);
				float labelPermSummerWidth = Text.CalcSize(labelPermSummer).x + 28f;
				Widgets.CheckboxLabeled(new Rect(right.width - labelPermSummerWidth, lineHeight * 2, labelPermSummerWidth, lineHeight),
										labelPermSummer, ref perm_summer);
				// Permanent winter
				string labelPermWinter = SeasonUtility.LabelCap(Season.PermanentWinter);
				float labelPermWinterWidth = Text.CalcSize(labelPermWinter).x + 28f;
				Widgets.CheckboxLabeled(new Rect(right.width - labelPermWinterWidth, lineHeight * 3, labelPermWinterWidth, lineHeight),
										labelPermWinter, ref perm_winter);
				this.song.SetAllowedSeasons(spring, summer, fall, winter, perm_summer, perm_winter);
			}
			// Allowed time of day
			{
				string labelDay = "Day".Translate();
				float labelDayWidth = Text.CalcSize(labelDay).x + 28f;
				if (Widgets.RadioButtonLabeled(new Rect(right.width / 3 - labelDayWidth, lineHeight * 3 + lineSkip,
														labelDayWidth, lineHeight), labelDay,
											   this.song.allowedTimeOfDay == TimeOfDay.Day))
					this.song.allowedTimeOfDay = TimeOfDay.Day;
				string labelNight = "CustomMusic_Night".Translate();
				float labelNightWidth = Text.CalcSize(labelNight).x + 28f;
				if (Widgets.RadioButtonLabeled(new Rect(right.width / 3 * 2 - labelNightWidth, lineHeight * 3 + lineSkip,
														labelNightWidth, lineHeight), labelNight,
											   this.song.allowedTimeOfDay == TimeOfDay.Night))
					this.song.allowedTimeOfDay = TimeOfDay.Night;
				string labelAny = "CustomMusic_Any".Translate();
				float labelAnyWidth = Text.CalcSize(labelAny).x + 28f;
				if (Widgets.RadioButtonLabeled(new Rect(right.width - labelAnyWidth, lineHeight * 3 + lineSkip,
														labelAnyWidth, lineHeight), labelAny,
											  this.song.allowedTimeOfDay == TimeOfDay.Any))
					this.song.allowedTimeOfDay = TimeOfDay.Any;
			}
			// Tense
			Widgets.Checkbox(right.width / 2 - 12f, lineHeight * 4 + lineSkip * 2, ref this.song.tense);
			// Commonality
			this.song.commonality = Widgets.HorizontalSlider(new Rect(0, lineHeight * 5 + lineSkip * 3, right.width,
																 lineHeight), this.song.commonality, 0, 1, false,
														"" + this.song.commonality, null, null, 0.01f);
			// Play on map
			Widgets.Checkbox(right.width / 2 - 12f, lineHeight * 6 + lineSkip * 4, ref this.song.playOnMap);
			// Volume
			this.song.volume = Widgets.HorizontalSlider(new Rect(0, lineHeight * 7 + lineSkip * 5, right.width,
																 lineHeight), this.song.volume, 0, 1, false,
														(int) (this.song.volume * 100f) + " %", null, null, 0.01f);
			GUI.EndGroup();
			Text.Anchor = TextAnchor.UpperLeft;
		}
	}
}
