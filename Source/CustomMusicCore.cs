﻿using Verse;
using RimWorld;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Profiling;
using HarmonyLib;

using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CustomMusic {
	public class CustomSongDef : SongDef, IExposable {
		public CustomSongDef() {
			this.allowedSeasons = new List<Season>(3);
		}

		public void ExposeData() {
			Scribe_Values.Look<string>(ref this.clipPath, "clipPath");
			Scribe_Values.Look<string>(ref this.defName, "defName");
			Scribe_Collections.Look<Season>(ref this.allowedSeasons, "allowedSeasons");
			Scribe_Values.Look<TimeOfDay>(ref this.allowedTimeOfDay, "allowedTimeOfDay", TimeOfDay.Any);
			Scribe_Values.Look<bool>(ref this.tense, "tense", false);
			Scribe_Values.Look<float>(ref this.commonality, "commonality", 1f);
			Scribe_Values.Look<bool>(ref this.playOnMap, "playOnMap", true);
			Scribe_Values.Look<float>(ref this.volume, "volume", 1f);

			if (Scribe.mode == LoadSaveMode.PostLoadInit)
				PostLoad();
		}

		public override void PostLoad() {
			if (this.defName == "UnnamedDef") {
				this.defName = Regex.Replace(Path.GetFileNameWithoutExtension(this.clipPath), @"[^\w-]", "_",
												RegexOptions.IgnoreCase);
			}
		}

		public void SetAllowedSeasons(bool spring, bool summer, bool fall, bool winter, bool perm_summer, bool perm_winter) {
			if (spring && summer && fall && winter && perm_summer && perm_winter) {
				if (!this.allowedSeasons.Contains(Season.Undefined)) {
					this.allowedSeasons.Clear();
					this.allowedSeasons.Add(Season.Undefined);
				}
			} else {
				this.allowedSeasons.Remove(Season.Undefined);
				if (spring) {
					if (!this.allowedSeasons.Contains(Season.Spring))
						this.allowedSeasons.Add(Season.Spring);
				} else
					this.allowedSeasons.Remove(Season.Spring);

				if (summer) {
					if (!this.allowedSeasons.Contains(Season.Summer))
						this.allowedSeasons.Add(Season.Summer);
				} else
					this.allowedSeasons.Remove(Season.Summer);

				if (fall) {
					if (!this.allowedSeasons.Contains(Season.Fall))
						this.allowedSeasons.Add(Season.Fall);
				} else
					this.allowedSeasons.Remove(Season.Fall);

				if (winter) {
					if (!this.allowedSeasons.Contains(Season.Winter))
						this.allowedSeasons.Add(Season.Winter);
				} else
					this.allowedSeasons.Remove(Season.Winter);

				if (perm_summer) {
					if (!this.allowedSeasons.Contains(Season.PermanentSummer))
						this.allowedSeasons.Add(Season.PermanentSummer);
				} else
					this.allowedSeasons.Remove(Season.PermanentSummer);

				if (perm_winter) {
					if (!this.allowedSeasons.Contains(Season.PermanentWinter))
						this.allowedSeasons.Add(Season.PermanentWinter);
				} else
					this.allowedSeasons.Remove(Season.PermanentWinter);
			}
		}

		public bool PlaysDuringSpring {
			get {
				return this.allowedSeasons.Contains(Season.Spring);
			}
		}

		public bool PlaysDuringSummer {
			get {
				return this.allowedSeasons.Contains(Season.Summer);
			}
		}

		public bool PlaysDuringFall {
			get {
				return this.allowedSeasons.Contains(Season.Fall);
			}
		}

		public bool PlaysDuringWinter {
			get {
				return this.allowedSeasons.Contains(Season.Winter);
			}
		}

		public bool PlaysDuringPermSummer {
			get {
				return this.allowedSeasons.Contains(Season.PermanentSummer);
			}
		}

		public bool PlaysDuringPermWinter {
			get {
				return this.allowedSeasons.Contains(Season.PermanentWinter);
			}
		}
	}

	public class MusicInfoCache
	{
		public SongDef lastSong = null;
		public String lastTitle = "";
		public float titleLength;
		public float scrollWidth;
		public Rect trackTitle;
		public Rect scrollOutRect;
		public Rect scrollViewRect;
		public Rect buttonRect;

		public bool updateNeeded = true;
		public bool learningHelperActive = false;

		public void Update()
		{
			if (Find.MusicManagerPlay != null)
			{
				lastSong = (SongDef)typeof(MusicManagerPlay)
						.GetField("lastStartedSong", BindingFlags.Instance | BindingFlags.NonPublic)
						.GetValue(Find.MusicManagerPlay);
				if (lastSong != null)
					lastTitle = Regex.Replace(Path.GetFileNameWithoutExtension(lastSong.defName), @"_", " ",
								RegexOptions.IgnoreCase);
			}

			float b = (float)UI.screenHeight / 2f + 12f;
			float a = 8f;

			if (CustomMusicCore.settings.respectLearningHelper && Find.PlaySettings.showLearningHelper)
			{
				a = (float)typeof(LearningReadout)
						.GetField("contentHeight", BindingFlags.Instance | BindingFlags.NonPublic)
						.GetValue(Find.Tutor.learningReadout) + 26f;
			}

			trackTitle = new Rect((float)UI.screenWidth - 208f, Mathf.Min(a, b), 200f, 26f);
			titleLength = Text.CalcSize(lastTitle).x;
			scrollWidth = Mathf.Max(.1f, titleLength - trackTitle.width - trackTitle.height);

			scrollOutRect = new Rect(0f, 0f, trackTitle.width, trackTitle.height);
			scrollViewRect = new Rect(0f, 0f, titleLength, trackTitle.height);
			buttonRect = new Rect(trackTitle.width - trackTitle.height + 1f, 0f, trackTitle.height - 1f, trackTitle.height - 1f);
		}
	}

	public class MusicSettings : ModSettings {
		public string musicPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
		public List<CustomSongDef> songs = new List<CustomSongDef>();

		public bool showSongInfo = false;
		public bool songInfoScroll = true;
		public bool respectLearningHelper = true;

		public override void ExposeData() {
			base.ExposeData();
			Scribe_Values.Look(ref this.showSongInfo, "showSongInfo", false);
			Scribe_Values.Look(ref this.songInfoScroll, "songInfoScroll", true);
			Scribe_Values.Look(ref this.respectLearningHelper, "respectLearningHelper", true);
			Scribe_Values.Look(ref this.musicPath, "musicPath",
							   Environment.GetFolderPath(Environment.SpecialFolder.Personal));
			Scribe_Collections.Look(ref this.songs, "songList", LookMode.Deep);
		}
	}

	[StaticConstructorOnStartup]
	class MusicMod : Mod {
		//
		// Fields
		//
		private readonly MusicSettings settings;
		protected static string invalidChars = new string(Path.GetInvalidPathChars());
		protected Vector2 scrollPos = Vector2.zero;
		protected float lineHeight = 24f;
		protected float margin = 4f;
		protected float scrollBarWidth = 16f;
		protected float cellMargin = 2f;
		protected float cellPadding = 2f;
		protected int buttonWidth = 24;
		protected float volumeFieldLength = 240f;

		//
		// Constructor
		//
		public MusicMod(ModContentPack content) : base(content) {
			this.settings = GetSettings<MusicSettings>();
			CustomMusicCore.settings = this.settings;
			DefDatabase<SongDef>.Add(settings.songs.Cast<SongDef>().ToList());
		}

		public override string SettingsCategory() => "Custom Music";

		public override void DoSettingsWindowContents(Rect inRect) {
			Rect controlRect = inRect.TopPart(0.1f);
			Rect pathRow = controlRect.TopHalf();
			Rect checkboxRow = controlRect.BottomHalf();
			Rect tableRect = inRect.BottomPart(0.9f);

			checkboxRow.y += margin;

			tableRect.y += margin * 2;
			tableRect.height -= margin * 2;

			GUI.BeginGroup(pathRow);
			{
				pathRow = new Rect(0, 0, pathRow.width, pathRow.height);
				Text.Anchor = TextAnchor.MiddleLeft;
				string label = "CustomMusic_musicPath".Translate();
				//Text.Anchor = TextAnchor.UpperLeft;
				float labelWidth = Text.CalcSize(label).x;
				Widgets.Label(new Rect(pathRow.x, pathRow.y, labelWidth, pathRow.height), label);
				Rect textFieldRect = new Rect(pathRow.x + labelWidth + margin, pathRow.y,
											  pathRow.RightPart(0.2f).x - margin - pathRow.x - labelWidth - margin,
											  pathRow.height);
				textFieldRect.width -= margin;
				this.settings.musicPath = Widgets.TextField(textFieldRect,
						this.settings.musicPath, int.MaxValue, new Regex("[^" + invalidChars + "]*"));
				if (Widgets.ButtonText(pathRow.RightPart(0.2f).LeftPart(0.49f), "CustomMusic_open".Translate()))
					Find.WindowStack.Add(new Dialog_MusicBrowser());
				if (Widgets.ButtonText(pathRow.RightPart(0.2f).RightPart(0.49f), "CustomMusic_update".Translate()))
					CustomMusicCore.ScanMusicPath();
			}
			GUI.EndGroup();
			GUI.BeginGroup(checkboxRow);
			{
				checkboxRow = new Rect(0, 0, checkboxRow.width, checkboxRow.height);
				Text.Anchor = TextAnchor.MiddleLeft;

				string songInfoScrollLabel = "CustomMusic_songInfoScroll".Translate();
				string respectLearningLabel = "CustomMusic_respectLearningHelper".Translate();

				const float checkboxWidth = 24f;
				float curX, labelWidth;

				labelWidth = Text.CalcSize(songInfoScrollLabel).x;
				Rect songInfoScrollRect = new Rect(checkboxRow.x, checkboxRow.y, labelWidth, checkboxRow.height);
				Widgets.Label(songInfoScrollRect, songInfoScrollLabel);
				Widgets.Checkbox(curX = checkboxRow.x + labelWidth + margin, checkboxRow.y, ref this.settings.songInfoScroll, paintable: true);

				songInfoScrollRect.width += margin + checkboxWidth;
				TooltipHandler.TipRegion(songInfoScrollRect, (TipSignal)"CustomMusic_songInfoScroll_desc".Translate());

				labelWidth = Text.CalcSize(respectLearningLabel).x;
				Rect respectLearningRect = new Rect(curX = curX + checkboxWidth + margin*2, pathRow.y, labelWidth, pathRow.height);
				Widgets.Label(respectLearningRect, respectLearningLabel);
				Widgets.Checkbox(curX + labelWidth + margin, checkboxRow.y, ref this.settings.respectLearningHelper, paintable: true);

				respectLearningRect.width += margin + checkboxWidth;
				TooltipHandler.TipRegion(respectLearningRect, (TipSignal)"CustomMusic_respectLearningHelper_desc".Translate());
			}
			GUI.EndGroup();
			GUI.BeginGroup(tableRect);
			{
				controlRect = new Rect(0, 0, controlRect.width - scrollBarWidth, lineHeight);
				WidgetRow tableTitles = new WidgetRow(0, 0, UIDirection.RightThenDown, controlRect.width, cellMargin);
				float titleFieldWidth;
				if (Current.ProgramState == ProgramState.Playing)
					titleFieldWidth = controlRect.width - cellMargin * 4 - buttonWidth * 3 - volumeFieldLength;
				else
					titleFieldWidth = controlRect.width - cellMargin * 3 - buttonWidth * 2 - volumeFieldLength;
				Text.Anchor = TextAnchor.MiddleCenter;
				tableTitles.Label("CustomMusic_title".Translate(), titleFieldWidth - 2f);
				if (Current.ProgramState == ProgramState.Playing)
					tableTitles.Icon(ContentFinder<Texture2D>.Get("UI/Buttons/Dev/Play"), "CustomMusic_preview_desc".Translate());
				tableTitles.Icon(ContentFinder<Texture2D>.Get("UI/Icons/Colonistbar/Attacking"), "CustomMusic_tense_desc".Translate());
				tableTitles.Label("CustomMusic_volume".Translate(), volumeFieldLength - 2f);
				tableTitles.Icon(ContentFinder<Texture2D>.Get("UI/Buttons/Settings"), "CustomMusic_settings".Translate());
				tableRect = new Rect(0, lineHeight + margin, tableRect.width, tableRect.height - lineHeight - margin);
				float tableHeight = (float) this.settings.songs.Count() * lineHeight;
				Widgets.BeginScrollView(tableRect, ref this.scrollPos, new Rect(0f, 0f, inRect.width - scrollBarWidth, tableHeight));
				int index = 0;
				Rect titleRect = new Rect(0f, 0f, titleFieldWidth, lineHeight);
				Rect previewRect = new Rect();
				Rect tenseRect;
				if (Current.ProgramState == ProgramState.Playing) {
					previewRect = new Rect(titleRect.x + titleRect.width + cellMargin, 0f, buttonWidth, lineHeight);
					tenseRect = new Rect(previewRect.x + previewRect.width + cellMargin, 0f, buttonWidth, lineHeight);
				} else
					tenseRect = new Rect(titleRect.x + titleRect.width + cellMargin, 0f, buttonWidth, lineHeight);
				Rect volumeRect = new Rect(tenseRect.x + tenseRect.width + cellMargin, 0f, volumeFieldLength, lineHeight);
				Rect settingsRect = new Rect(volumeRect.x + volumeRect.width + cellMargin, 0f, buttonWidth, lineHeight);
				foreach (CustomSongDef song in this.settings.songs) {
					// Draw brighter background every other line
					if (index % 2 == 0) {
						Widgets.DrawAltRect(titleRect);
						if (Current.ProgramState == ProgramState.Playing)
							Widgets.DrawAltRect(previewRect);
						Widgets.DrawAltRect(tenseRect);
						Widgets.DrawAltRect(volumeRect);
						Widgets.DrawAltRect(settingsRect);
					}
					Rect titleCont = new Rect(titleRect.x + cellPadding, titleRect.y,
											  titleRect.width - cellPadding * 2, titleRect.height);
					Text.Anchor = TextAnchor.MiddleLeft;
					Widgets.Label(titleCont, Path.GetFileNameWithoutExtension(song.clipPath));
					if (Current.ProgramState == ProgramState.Playing) {
						Rect previewCont = new Rect(previewRect);
						previewCont.position += new Vector2(cellPadding, cellPadding);
						previewCont.size -= new Vector2(cellPadding * 2, cellPadding * 2);
						if (Widgets.ButtonImage(previewCont, ContentFinder<Texture2D>.Get("UI/Buttons/Dev/Play")))
							Find.MusicManagerPlay.ForceStartSong(song, false);
					}
					Widgets.Checkbox(tenseRect.position + new Vector2(cellPadding, cellPadding), ref song.tense, 20f);
					Rect volumeCont = new Rect(volumeRect.x + cellPadding, volumeRect.y,
											   volumeRect.width - cellPadding * 2, volumeRect.height);
					song.volume = Widgets.HorizontalSlider(volumeCont.LeftPart(0.79f), song.volume, 0, 1, true, null,
														   null, null, 0.01f);
					Text.Anchor = TextAnchor.MiddleRight;
					Widgets.Label(volumeCont.RightPart(0.2f), (int) (song.volume * 100f) + " %");
					Rect settingsCont = new Rect(settingsRect);
					settingsCont.position += new Vector2(cellPadding, cellPadding);
					settingsCont.size -= new Vector2(cellPadding * 2, cellPadding * 2);
					if (Widgets.ButtonImage(settingsCont, ContentFinder<Texture2D>.Get("UI/Buttons/OpenSpecificTab")))
						Find.WindowStack.Add(new Dialog_AdvancedSettings(song));
					titleRect.y = previewRect.y = tenseRect.y = volumeRect.y = settingsRect.y += lineHeight;
					index++;
				}
				Widgets.EndScrollView();
			}
			GUI.EndGroup();
			// Reset Text.Anchor to default
			Text.Anchor = TextAnchor.UpperLeft;
			this.settings.Write();
		}
	}

	public class CustomMusicCore {
		public static MusicSettings settings;
		public static MusicInfoCache musicInfoCache = new MusicInfoCache();
		private static Vector2 scrollPos = Vector2.zero;

		public CustomMusicCore() {
			Current.Root_Play.musicManagerPlay = new MusicManagerPlay();
		}

		public static void ScanMusicPath() {
			if (!Directory.Exists(settings.musicPath)) {
				Find.WindowStack.Add(new Dialog_MessageBox("CustomMusic_dirNotExist".Translate()));
				return;
			}
			string[] files = Directory.GetFiles(settings.musicPath, "*.ogg", SearchOption.AllDirectories);
			IEnumerable<string> newFiles = from file in files where settings.songs.FindIndex(
				(song) => song.clipPath == file) == -1 select file;
			IEnumerable<string> removedFiles = from song in settings.songs.Select(
				(song) => song.clipPath).ToList() where !files.Contains(song) select song;
			foreach (string file in newFiles) {
				settings.songs.Add(new CustomSongDef());
				CustomSongDef current = settings.songs.Last();
				current.clipPath = file;
				current.PostLoad();
				DefDatabase<SongDef>.Add(current);
			}
			foreach (string file in removedFiles) {
				typeof(DefDatabase<SongDef>).GetMethod("Remove", BindingFlags.Static | BindingFlags.NonPublic)
											.Invoke(null, new object[] { settings.songs.Find((song) => song.clipPath == file) });
			}
			settings.songs.RemoveAll((song) => removedFiles.Contains(song.clipPath));
		}

		public static void MusicInfoOnGUI() {
			if (musicInfoCache.updateNeeded || 
				(CustomMusicCore.settings.respectLearningHelper && 
				Find.PlaySettings.showLearningHelper != musicInfoCache.learningHelperActive))
			{
				musicInfoCache.Update();
				musicInfoCache.updateNeeded = false;
			}

			if (CustomMusicCore.settings.showSongInfo) {
				GUI.BeginGroup(musicInfoCache.trackTitle);

				if (CustomMusicCore.settings.songInfoScroll) {
					Widgets.BeginScrollView(musicInfoCache.scrollOutRect,
							ref scrollPos, musicInfoCache.scrollViewRect, false);
					scrollPos.x += .25f * Mathf.Sign(Mathf.Sin(Find.TickManager.TicksGame * Mathf.PI / (musicInfoCache.scrollWidth * 4)));
					Widgets.Label(musicInfoCache.scrollViewRect, musicInfoCache.lastTitle);
					Widgets.EndScrollView();
				}
				else {
					Widgets.Label(musicInfoCache.scrollViewRect, musicInfoCache.lastTitle);

					if (Mouse.IsOver(musicInfoCache.scrollViewRect))
						Widgets.DrawHighlight(musicInfoCache.scrollViewRect);
					TooltipHandler.TipRegion(musicInfoCache.scrollViewRect, musicInfoCache.lastTitle);
				}

				if (musicInfoCache.lastSong is CustomSongDef song) {
					Widgets.ButtonImage(musicInfoCache.buttonRect, ContentFinder<Texture2D>.Get("UI/Buttons/OpenSpecificTab"));
					if (Mouse.IsOver(musicInfoCache.buttonRect)) {
						if (Input.GetMouseButtonDown(0)) {
							Find.WindowStack.Add(new Dialog_AdvancedSettings(song));
						}
					}
				}
				GUI.EndGroup();
			}
		}
	}

	[StaticConstructorOnStartup]
	static class HarmonyPatches {
		static HarmonyPatches() {
			Harmony harmony = new Harmony("rimworld.chais.custommusic");

			MethodInfo chooseNextSong = AccessTools.Method(typeof(MusicManagerPlay), "ChooseNextSong");
			HarmonyMethod postfixNext = new HarmonyMethod(typeof(HarmonyPatches).GetMethod("ChooseNextSong_Postfix"));
			harmony.Patch(chooseNextSong, null, postfixNext);

			MethodInfo playSettingsGlobalControls = AccessTools.Method(typeof(PlaySettings), "DoPlaySettingsGlobalControls");
			HarmonyMethod postfixPlaySettings = new HarmonyMethod(typeof(HarmonyPatches).GetMethod("DoPlaySettingsGlobalControls_Postfix"));
			harmony.Patch(playSettingsGlobalControls, null, postfixPlaySettings);

			MethodInfo uiRootOnGUI = AccessTools.Method(typeof(UIRoot_Play), "UIRootOnGUI");
			HarmonyMethod postfixUiRootOnGUI = new HarmonyMethod(typeof(HarmonyPatches).GetMethod("UIRootOnGUI_Prefix"));
			harmony.Patch(uiRootOnGUI, null, postfixUiRootOnGUI);
		}

		private static void GetAudioClip(string source, SongDef songDef) {
			using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(source, AudioType.OGGVORBIS)) {
				DownloadHandlerAudioClip handler = new DownloadHandlerAudioClip(string.Empty, AudioType.OGGVORBIS);
				handler.streamAudio = true;
				www.downloadHandler = handler;
				UnityWebRequestAsyncOperation operation = www.SendWebRequest();
				while (www.downloadProgress < 0.01f) {
					new WaitForSeconds(0.1f);
				}
				songDef.clip = handler.audioClip;
				operation.completed += (asyncOperation) => {
					handler.Dispose();
					www.Dispose();
				};
			}
		}

		public static void ChooseNextSong_Postfix(MusicManagerPlay __instance, ref SongDef __result) {
			if (__result is CustomSongDef song)
				GetAudioClip("file://" + song.clipPath, __result);
			CustomMusicCore.musicInfoCache.updateNeeded = true;
		}

		public static void DoPlaySettingsGlobalControls_Postfix(PlaySettings __instance, ref WidgetRow row, ref bool worldView) {
			if (!worldView) {
				row.ToggleableIcon(ref CustomMusicCore.settings.showSongInfo,
						ContentFinder<Texture2D>.Get("UI/Buttons/ShowMusicInfo"),
						"CustomMusic_showMusicInfo".Translate(),
						SoundDefOf.Mouseover_ButtonToggle);
			}
		}

		public static void UIRootOnGUI_Prefix(UIRoot_Play __instance) {
			Profiler.BeginSample("MusicInfoOnGUI()");
			CustomMusicCore.MusicInfoOnGUI();
			Profiler.EndSample();
		}
	}
}
